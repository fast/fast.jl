# SPDX-License-Identifier: CECILL-2.1

function corpora()
    lexs = Dict()
    for lexiconfile in glob("*/*/lexicon.txt", CONFIG["resources"]["corpora"])
        lexname = lexiconfile |> dirname |> basename
        lang = lexiconfile |> dirname |> dirname |> basename
        list = get(lexs, lang, [])
        lexs[lang] = push!(list, lexname)
    end

    if isempty(lexs)
        printstyled("no lexicon available\n", color = :red)
    end

    for lang in sort(collect(keys(lexs)))
        printstyled(lang, "\n"; bold = true, color = :light_blue)
        for lexname in sort(lexs[lang])
            printstyled("  $lexname\n"; color = :green)
        end
    end
end

