# SPDX-License-Identifier: CECILL-2.1

function make_hmm_fsa(topo, startpdf)
    T = double_precision() ? Float64 : Float32
	K = LogSemiring{T}
	Q = length(topo["states"])

	I, V = [], K[]
	for s in topo["states"]
		w = get(s, "initweight", zero(T))
		if ! iszero(w)
			push!(I, s["id"])
			push!(V, K(log(w)))
		end
	end
	α_ = sparsevec(I, V, Q)

	I, J, V = [], [], K[]
	for s in topo["transitions"]
		w = get(s, "weight", zero(T))
		if ! iszero(w)
			push!(I, s["src"])
			push!(J, s["dest"])
			push!(V, K(log(w)))
		end
	end
	T_ = sparse(I, J, V, Q, Q)

	I, V = [], K[]
	for s in topo["states"]
		w = get(s, "finalweight", zero(T))
		if ! iszero(w)
			push!(I, s["id"])
			push!(V, K(log(w)))
		end
	end
	ω_ = sparsevec(I, V, Q)

	λ_ = collect((1:Q) .+ (startpdf-1))

	FSA(α_, T_, ω_, zero(K), λ_)
end

function get_topology(_tags)
	tags = Vector(_tags)
	topo = Fast.CONFIG["hmm_topo"]
	while ! isempty(tags) && haskey(topo, tags[1])
		tag = pop!(tags)
		topo = topo[tag]
	end
	topo
end

function generate_unit_mapfile(path, lexicon)
    unitset = Set()
    for (k, pronunciation_fsa) in lexicon
        union!(unitset, Set(λ(pronunciation_fsa)))
    end

    open(path, "w") do f
        nunit = 1
        no_speech_unit = CONFIG["lexicon"]["no_speech_unit"]
        if ! isempty(no_speech_unit)
            println(f, no_speech_unit, "\t", nunit, "\t", "non-speech-unit")
            nunit += 1
        end

        for unit in sort(collect(unitset))
            println(f, unit, "\t", nunit, "\t", "speech-unit")
            nunit += 1
        end
    end
end

function makehmms(lexicon::Lexicon)
    lexdir = joinpath(CONFIG["resources"]["lexicons"], lexicon.lang, lexicon.name)
    mapfile = joinpath(lexdir, "units.txt")
    if ! isfile(mapfile)
        @debug "no file $mapfile, creating a default unit map file"
        generate_unit_mapfile(mapfile, lexicon)
    else
        @debug "using unit file: $mapfile"
    end

    hmms = Dict()
    startpdf = 1
    open(mapfile, "r") do f
        for line in eachline(f)
            tokens = split(line)
            unit, unitid, tags = tokens[1], tokens[2], tokens[3:end]

            hmms[unit] = make_hmm_fsa(get_topology(tags), startpdf)
            startpdf += nstates(hmms[unit])
        end
    end
    hmms
end

function align_fsa(seq::AbstractVector)
    T = double_precision() ? Float64 : Float32
	K = LogSemiring{T}

    no_speech_word = CONFIG["lexicon"]["no_speech_word"]

	sup_conf = get(CONFIG, "supervision", Dict())
	ali_conf = get(sup_conf, "align_graph", Dict())
	sil_prob_begin = get(ali_conf, "prob_sil_begin", 0)
	sil_prob_end = get(ali_conf, "prob_sil_end", 0)
	sil_prob_between = get(ali_conf, "prob_sil_between", 0)

    nstates = 0
    I_α, V_α = [], K[]
    I_T, J_T, V_T = [], [], K[]
    I_ω, V_ω = [], K[]
    λ_ = []
    for (i, x) in enumerate(seq)
        if i == 1 && iszero(sil_prob_begin)
            nstates += 1
            push!(λ_, x)
            push!(I_α, nstates)
            push!(V_α, K(log(sil_prob_begin)))
        elseif i == 1
            nstates += 1
            push!(λ_, no_speech_word)
            push!(I_α, nstates)
            push!(V_α, K(log(sil_prob_begin)))

            nstates += 1
            push!(λ_, x)
            push!(I_α, nstates)
            push!(V_α, K(log(1 - sil_prob_begin)))

            push!(I_T, nstates - 1)
            push!(J_T, nstates)
            push!(V_T, one(K))
        elseif i > 1 && iszero(sil_prob_between)
            nstates += 1
            push!(λ_, x)
            push!(I_T, nstates - 1)
            push!(J_T, nstates)
            push!(V_T, one(K))
        elseif i > 1
            nstates += 1
            push!(λ_, no_speech_word)
            push!(I_T, nstates - 1)
            push!(J_T, nstates)
            push!(V_T, K(log(sil_prob_between)))

            nstates += 1
            push!(λ_, x)
            push!(I_T, nstates - 2)
            push!(J_T, nstates)
            push!(V_T, K(log(1 - sil_prob_between)))

            push!(I_T, nstates - 1)
            push!(J_T, nstates)
            push!(V_T, one(K))
        end
    end

    if iszero(sil_prob_end)
        push!(I_ω, nstates)
        push!(V_ω, one(K))
    else
        push!(I_ω, nstates)
        push!(V_ω, K(log(1 - sil_prob_end)))

        nstates += 1
        push!(λ_, no_speech_word)
        push!(I_T, nstates - 1)
        push!(J_T, nstates)
        push!(V_T, K(log(sil_prob_end)))

        push!(I_ω, nstates)
        push!(V_ω, one(K))
    end

    FSA(
        sparsevec(I_α, V_α, nstates),
        sparse(I_T, J_T, V_T, nstates, nstates),
        sparsevec(I_ω, V_ω, nstates),
        zero(K),
        λ_
    )
end

