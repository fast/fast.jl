# SPDX-License-Identifier: CECILL-2.1

module Fast

using FiniteStateAutomata
using Semirings
using SparseArrays
using SpeechFeatures
using SpeechCorpora
using TOML
using Glob

export
    lexicons,
    Lexicon,
    makehmms,
    align_fsa,
    get_mfcc_hires,
    SpeechDataset


include("setup.jl")
include("lexicon.jl")
include("hmm.jl")
include("features.jl")
include("dataset.jl")

end

