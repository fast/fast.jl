# SPDX-License-Identifier: CECILL-2.1

const windowfn = Dict(
    "hann" => SpeechFeatures.HannWindow,
    "hamming" => SpeechFeatures.HammingWindow,
    "rectangular" => SpeechFeatures.RectangularWindow
)

function get_mfcc_hires(r::Recording, s::Supervision)
    fea_conf = get(Fast.CONFIG, "features", Dict())
    mfcc_conf = get(fea_conf, "mfcc_hires", Dict())
    x, fs = load(r, s)
    S, fftlen = stft(x[:,1];
        srate = get(mfcc_conf, "srate", fs),
        dithering = get(mfcc_conf, "dithering", 0),
        removedc = get(mfcc_conf, "removedc", true),
        frameduration = get(mfcc_conf, "frameduration", 0.025),
        framestep = get(mfcc_conf, "framestep", 0.01),
        windowfn = windowfn[get(mfcc_conf, "windowfn", "hann")],
        windowexp = get(mfcc_conf, "windowexp", 0.85),
        preemph = get(mfcc_conf, "preemph", 0)
    )
    fbank = filterbank(
        get(mfcc_conf, "nfilters", 40);
        fftlen,
        srate = get(mfcc_conf, "srate", fs),
        lofreq = get(mfcc_conf, "lofreq", 0),
        hifreq = (get(mfcc_conf, "state", fs)/ 2) - get(mfcc_conf, "higreq", 0) - 1
    )
    mS = fbank * abs.(S)
    mfcc(mS;
        nceps = get(mfcc_conf, "nceps", 40),
        liftering = get(mfcc_conf, "liftering", 22)
    )
end
