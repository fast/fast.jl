# SPDX-License-Identifier: CECILL-2.1

struct Lexicon <: AbstractDict{Any,Any}
    lang
    name
    data::Dict{Any,Any}
end

Base.iterate(l::Lexicon) = iterate(l.data)
Base.iterate(l::Lexicon, state) = iterate(l.data, state)
Base.getindex(l::Lexicon, k) = getindex(l.data, k)
Base.setindex!(l::Lexicon, k, v) = setindex!(l.data, k, v)
Base.get(l::Lexicon, k, d) = get(l.data, k, d)
Base.haskey(l::Lexicon, k) = haskey(l.data, k)
Base.length(l::Lexicon) = length(l.data)

function lexicons()
    lexs = Dict()
    for lexiconfile in glob("*/*/lexicon.txt", CONFIG["resources"]["lexicons"])
        lexname = lexiconfile |> dirname |> basename
        lang = lexiconfile |> dirname |> dirname |> basename
        list = get(lexs, lang, [])
        lexs[lang] = push!(list, lexname)
    end

    if isempty(lexs)
        printstyled("no lexicon available\n", color = :red)
    end

    for lang in sort(collect(keys(lexs)))
        printstyled(lang, "\n"; bold = true, color = :light_blue)
        for lexname in sort(lexs[lang])
            printstyled("  $lexname\n"; color = :green)
        end
    end
end

function pronunciation_fsa(pronun)
    T = double_precision() ? Float64 : Float32
	K = LogSemiring{T}
    Q = length(pronun)

    FSA(
        sparsevec([1], one(K), Q),
        sparse(collect(1:Q-1), collect(2:Q), one(K), Q, Q),
        sparsevec([Q], one(K), Q),
        zero(K),
        pronun
    ) |> AcyclicFSA
end

function Lexicon(lang, name)
    path = joinpath(CONFIG["resources"]["lexicons"], lang, name, "lexicon.txt")

    lexicon = Lexicon(lang, name, Dict())

    open(path, "r") do f
        for line in eachline(f)
            tokens = split(line)
            word = tokens[1]
            list = get(lexicon, word, [])
            push!(list, pronunciation_fsa(tokens[2:end]))
            lexicon[word] = list
        end
    end
    lexicon[CONFIG["lexicon"]["no_speech_word"]] =
        [pronunciation_fsa([CONFIG["lexicon"]["no_speech_unit"]])]
    lexicon[CONFIG["lexicon"]["oov_word"]] =
        [pronunciation_fsa([CONFIG["lexicon"]["oov_unit"]])]

    lexicon_minimized = Dict()
    for (word, pronuns) in lexicon
        lexicon_minimized[word] = union(pronuns...) |> minimize |> globalrenorm
    end

    Lexicon(lang, name, lexicon_minimized)
end

