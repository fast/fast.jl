# SPDX-License-Identifier: CECILL-2.1

struct SpeechDataset
    fn::Function
    recordings::AbstractDict
    supervisions::AbstractVector
end

SpeechDataset(fn::Function, recs::AbstractDict, sups::AbstractDict) =
    SpeechDataset(fn, recs, collect(values(sups)))

Base.length(ds::SpeechDataset) = length(ds.supervisions)

function Base.getindex(ds::SpeechDataset, idx)
    segment = ds.supervisions[idx]
    ds.fn(ds.recordings[segment.recording_id], segment)
end


