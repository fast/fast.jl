# SPDX-License-Identifier: CECILL-2.1

const FAST_RESOURCES_DEFAULTS = Dict(
    "lexicons" => "\"" * joinpath(homedir(), "lexicons") * "\"",
    "corpora" => "\"" * joinpath(homedir(), "corpora") * "\"",
)

const FAST_CORE_DEFAULTS = Dict(
    "double_precision" => false
)

const FAST_LEXICON_DEFAULTS = Dict(
    "no_speech_unit" => "<no-speech-unit>",
    "no_speech_word" => "<no-speech>"
)

function write_defaults(path)
    open(path, "w") do f
        println(f, "# FAST conffiguration file\n")

        println(f, "[core]")
        for (k, v) in FAST_CORE_DEFAULTS
            println(f, k, " = ", v)
        end

        println(f, "[lexicon]")
        for (k, v) in FAST_LEXICON_DEFAULTS
            println(f, k, " = ", v)
        end

        println(f, "[resources]")
        for (k, v) in FAST_RESOURCES_DEFAULTS
            println(f, k, " = ", v)
        end
    end
end

const CONFIG = Dict(
    "resources" => FAST_RESOURCES_DEFAULTS,
    "core" => FAST_CORE_DEFAULTS,
    "lexicon" => FAST_LEXICON_DEFAULTS
)

function __init__()
    configfile = get(ENV, "FAST_CONFIG_FILE", joinpath(homedir(), ".fastconfig"))
    local_configfile = joinpath(pwd(), ".fastconfig")

    if ! isfile(configfile) && ! isfile(local_configfile)
        @debug "no configuration file found, generating a default one"
        write_defaults(configfile)
    end

    merge!(CONFIG, TOML.parsefile(configfile))
    if isfile(local_configfile)
        @debug "overriding global config with local config file: $local_configfile"
        merge!(CONFIG, TOML.parsefile(local_configfile))
    end

    @debug "configuration file path: $configfile" CONFIG
end

double_precision() = CONFIG["core"]["double_precision"]

