# SPDX-License-Identifier: CECILL-2.1

using Fast
using SpeechCorpora

#######################################################################

const corpus = MiniLibriSpeech()
const lexicon_name = "cmudict-0.7b"
const outdir = "./"

#######################################################################

corpus |> download |> prepare

# Lexicon
lexicon = Fast.load(Lexicon, corpus.lang, lexicon_name)
@info "lexicon has $(length(lexicon)) entries"

# HMMs
hmms = makehmms(lexicon)
@show hmms

# Pronunciation automata from the lexicon
# L_fsa = make_pronunciationfsa(lexicon)

#
# model = Model(numpdf)

# train!(model, corpus)

