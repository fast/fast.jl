### A Pluto.jl notebook ###
# v0.19.22

using Markdown
using InteractiveUtils

# ╔═╡ ec7b5828-bce3-11ed-09db-7fe261ad056b
begin
	using Pkg
	Pkg.activate("./")

	using Revise
	using Fast
	using FiniteStateAutomata
	using Plots
	using SpeechCorpora
	using SpeechFeatures

	# Uncomment and re-run this cell to reload the configuration.
	#Fast.__init__()
	
	SpeechCorpora.setrootdir(Fast.CONFIG["resources"]["corpora"]);
end

# ╔═╡ 77aa9ab5-101e-4b96-ba83-c9a98f6a7348
md"""
# Maximum Mutual Information Training with FAST
*Lucas Ondel, LISN, CNRS*

This notebook shows how to train a neural network ASR with the Maximum Mutual Information Loss.


!!! info
	You need to add the [FAST package registry](https://gitlab.lisn.upsaclay.fr/fast/registry) to your Julia installation before to run this notebook.
"""

# ╔═╡ 51a47d85-1e63-4ffa-b9f5-5e7865385812
md"""
## Settings

When loading the FAST (i.e. `julia> using Fast`) configuration settings are loaded from the global configuration file (`~/.fastconfig`) and the local configuration file `./.fastconfig`. Settings defined in the local configuration will override their global definition. when loading the package. If none of these file exists, a global configuration file is generated. 

!!! tip
	You can reload the configuration settings by running. 
	> Fast.\__init__()
"""

# ╔═╡ b6cb83bd-038a-44a2-b1c9-57ce9f8d1e48
Fast.CONFIG

# ╔═╡ 0ef1e0f5-9481-4756-8745-73ed9409a691
Fast.CONFIG["resources"]

# ╔═╡ 562ffb4b-e0e6-4be0-bd2b-3ada0ce4fdee
md"""
## Corpus

Calling the function
> const corpus = CorpusName() 
will automatically download and prepare the corpus. 
"""

# ╔═╡ 981f6558-47cf-4296-b534-16fd8298d18c
# ╠═╡ show_logs = false
const corpus = MiniLibriSpeech()

# ╔═╡ c1c234de-a257-4955-b40b-5af0a3d37d88
md"""
## Pronunciation Lexicon

You need to provide a lexicon at the path $(Fast.CONFIG["resources"]["lexicons"])/lang/name/lexicon.txt where `lang` is the language of the corpus and `name` is a user-defined identifier name of the lexicon (e.g. `cmudict-xx`).
"""

# ╔═╡ 606e29a0-1d7f-49b7-b186-b815682f3b44
const lexicon_name = "cmudict-0.7b"

# ╔═╡ 1de3f94a-3b99-4c60-a686-23ca1c7d992d
lexicon = Lexicon(corpus.lang, "cmudict-0.7b")

# ╔═╡ e341ad89-5c2a-40d2-b0d9-3889320f6765
md"""
## Phonetic Units 

"""

# ╔═╡ c56dc2b4-be77-47a2-9b8a-62a9d28aeb8d
hmms = makehmms(lexicon)

# ╔═╡ 2fd7019f-5d27-4524-b189-9e06836f5a67
replace(lexicon["ENCHANTER'S"]) do q
	hmms[λ(lexicon["ENCHANTER'S"])[q]]
end 

# ╔═╡ 7d4c34db-e8a4-48ea-9199-5fb6b3ccd2e1
lexicon["<no-speech>"]

# ╔═╡ 4e5f0c03-1a88-4b56-8621-0b9ab82236ef
md"""
## Features
"""

# ╔═╡ e3b9ed3b-9c89-4b14-a3e8-3a5fa8761df8
fea = Fast.get_mfcc_hires(corpus.recordings["1988-147956-0027"], corpus.dev["1988-147956-0027"])

# ╔═╡ 213d1725-7ec2-41f2-8b92-6d31b4905714
heatmap(fea; xlabel="time (s)", ylabel="MFCC")

# ╔═╡ dfb30410-79c8-4341-872b-6205db857997
seq = split(corpus.train["6272-70171-0009"].data["text"])

# ╔═╡ 069ed577-bab3-4790-bdb9-fab99433e86f
ds = SpeechDataset(corpus.recordings, corpus.train) do rec, sup
	fea = get_mfcc_hires(rec, sup)
	G = align_fsa(seq)
	GL = replace(G) do q
		label = λ(G)[q]
		if label in keys(lexicon)
			lexicon[λ(G)[q]] 
		else
			lexicon[Fast.CONFIG["lexicon"]["oov_word"]]
		end
	end 
	
	GLH = replace(GL) do q
		hmms[last(λ(GL)[q])]
	end
	fea, GLH
end

# ╔═╡ 54baf4f1-6ecc-4218-866d-d5c526c1027d
ds[1]

# ╔═╡ 244fd118-79b9-431c-b9b4-bccb75d3c551
G_u = align_fsa(seq)

# ╔═╡ 9bb73540-a090-4cf8-b71e-6ee23390239b
GL_u = replace(G_u) do q
	label = λ(G_u)[q]
	if label in keys(lexicon)
		lexicon[λ(G_u)[q]] 
	else
		lexicon["<oov>"]
	end
end 

# ╔═╡ a70a9577-9971-4757-8990-e6e65bd8bd9e
GLH_u = replace(GL_u) do q
	hmms[last(λ(GL_u)[q])]
end

# ╔═╡ cfa4d700-14ec-40c1-8562-08ab6860567d
λ(GLH_u)

# ╔═╡ Cell order:
# ╟─77aa9ab5-101e-4b96-ba83-c9a98f6a7348
# ╟─51a47d85-1e63-4ffa-b9f5-5e7865385812
# ╠═ec7b5828-bce3-11ed-09db-7fe261ad056b
# ╠═b6cb83bd-038a-44a2-b1c9-57ce9f8d1e48
# ╠═0ef1e0f5-9481-4756-8745-73ed9409a691
# ╟─562ffb4b-e0e6-4be0-bd2b-3ada0ce4fdee
# ╠═981f6558-47cf-4296-b534-16fd8298d18c
# ╟─c1c234de-a257-4955-b40b-5af0a3d37d88
# ╠═606e29a0-1d7f-49b7-b186-b815682f3b44
# ╠═1de3f94a-3b99-4c60-a686-23ca1c7d992d
# ╟─e341ad89-5c2a-40d2-b0d9-3889320f6765
# ╠═c56dc2b4-be77-47a2-9b8a-62a9d28aeb8d
# ╠═2fd7019f-5d27-4524-b189-9e06836f5a67
# ╠═7d4c34db-e8a4-48ea-9199-5fb6b3ccd2e1
# ╟─4e5f0c03-1a88-4b56-8621-0b9ab82236ef
# ╠═e3b9ed3b-9c89-4b14-a3e8-3a5fa8761df8
# ╠═213d1725-7ec2-41f2-8b92-6d31b4905714
# ╠═069ed577-bab3-4790-bdb9-fab99433e86f
# ╠═54baf4f1-6ecc-4218-866d-d5c526c1027d
# ╠═dfb30410-79c8-4341-872b-6205db857997
# ╠═244fd118-79b9-431c-b9b4-bccb75d3c551
# ╠═9bb73540-a090-4cf8-b71e-6ee23390239b
# ╠═a70a9577-9971-4757-8990-e6e65bd8bd9e
# ╠═cfa4d700-14ec-40c1-8562-08ab6860567d
